
/* Enum representing the available playing card suits */
enum CardSuit {
	CS_HEARTS,
	CS_DIAMONDS,
	CS_SPADES,
	CS_CLUBS
};

/* Enum representing the available playing card ranks */
enum CardRank {
	CR_TWO = 2,
	CR_THREE = 3,
	CR_FOUR = 4,
	CR_FIVE = 5,
	CR_SIX = 6,
	CR_SEVEN = 7,
	CR_EIGHT = 8,
	CR_NINE = 9,
	CR_TEN = 10,
	CR_JACK = 11,
	CR_QUEEN = 12,
	CR_KING = 13,
	CR_ACE = 14
};

/* Represents a playing Card in memory */
struct PlayingCard {
	CardSuit Suit;
	CardRank Rank;
};

/* Main entry point for the Lab Exercise 2 Application */
int main()
{
	return 0;
}
